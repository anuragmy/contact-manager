# contact-manager
A Contact Manager Single Page App to add, update and delete contacts using React and Redux

# link 
https://mycontact-manager.herokuapp.com/

# to run project 
1) clone repository

2) npm install 

3) npm  start 
